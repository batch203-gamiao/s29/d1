// Advance Query Operators
	//  We want more flexible querying of data within MongoDB.
// [SECTION] Comparison Query Operators
	// $gt/$gte operator
	/*
		- Allows us to find documents that have field number values greater than or equal to specified value.
		- Syntax:
			db.collectionName.find({field:{$gt/$gte: value}});
	*/

	db.users.find(
		{
			age: {
				$gt: 65
			}
		}
	);

	db.users.find(
		{
			age: {
				$gte: 65
			}
		}
	);
	// $lt/$lte operator
	/*
		- Allows us to find documents that have field number values less than or equal to specified value.
		- Syntax:
			db.collectionName.find({field:{$lt/$lte: value}});
	*/

	db.users.find(
		{
			age: {
				$lt: 65
			}
		}
	);

	db.users.find(
		{
			age: {
				$lte: 65
			}
		}
	);

	// $ne operator
	/*
		- Allows us to find documents field number not equal to specified value.
		- Syntax:
			db.collectionName.find({field:{$ne: value}});
	*/

	db.users.find({age: {$ne: 82}})

	// $in operator
	/*
		- Allows us to find documents with specific match criteria one with one field using different values.
		- Syntax:
			db.collectionName.find({field:{$in:[valueA, valueB] }});
	*/

	db.users.find(
		{
			lastName: {$in: ["Hawking","Doe"]}
		}
	);

	db.users.find(
		{
			courses: {$in: ["HTML","React"]}
		}
	);

// [SECTION] Logical Query Operators
// $or operator
/*
	- Allows us to find documents that match a single criteria from multiple provide search criteria
	- Syntax:
		db.collectionName.find({ $or: [{fieldA:valueA}, {fieldB:valueB}]});
*/

	// Multiple field value pairs
	db.users.find(
		{
			$or:[
				{firstName: "Neil"},
				{age: 25}
			]	
		}
	);

	db.users.find(
		{
			$or:[
				{firstName: "Neil"},
				{age: {$gt: 25}}
			]	
		}
	);

// $and operator
/*
	- Allows us to find documents matching multiple criteria in a single field.
	- Syntax:
		db.collectionName.find({$and: [{fieldA: valueA},{fieldB:valueB}]});
*/

db.users.find(
	{
		$and:[
				{age: {$ne: 82}},
				{age: {$ne: 76}}
		]
	}
);

db.users.find(
	{
		$and:[
				{age: {$ne: 82}},
				{department: "Operations"}
		]
	}
);

// [SECTION] Field Projection
// To help with the readability of the values returned, we can include/exclude fields from the response.

// Inclusion
/*
	- Allows us to include/add specific fields only when retrieving a document.
	- The value provided is 1 to denote that the field is being included in the retrieval.
	- Syntax:
		db.collectionName.find({criteria}, {field: 1});
*/

// Exclusion

db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact: 0,
		department: 0
	}
)

// Supressing the ID field
/* 
	- When using field projection, field inclusion and exclusion may not be used at the same time.
	- Excluding the "_id" field is the only exception to this rule.
	- Syntax:
		db.collectionName.find({criteria},{_id: 0})
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
)

// [SECTION] Evalutaion Query Operators
// $regex operator
/*
	- Allows us to find documents that match a specific string pattern using regular expressions.
	- Syntax: 
		db.users.find({field: $regex: "pattern", $options: $optionValue});
*/

	// case sensitive query
	db.users.find(
		{	firstName: {
				$regex: 'N',
				$options: '$i'
			}
		}
	)

	db.users.find(
		{	firstName: {
				$regex: 'jane',
				$options: '$i'
			}
		}
	)